//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EnglishBoxSite.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kullanicilar
    {
        public int KullaniciID { get; set; }
        public string KullaniciAdi { get; set; }
        public string KullaniciSoyadi { get; set; }
        public string KullaniciEmail { get; set; }
        public string KullaniciOkul { get; set; }
        public string KullaniciMeslek { get; set; }
        public Nullable<System.DateTime> KullaniciKayitTarihi { get; set; }
        public string KullaniciSifre { get; set; }
        public string KullaniciRol { get; set; }
        public string KullaniciAktivasyon { get; set; }
        public Nullable<bool> Onay { get; set; }
        public string yeniSifre { get; set; }
        public string yeniSifreOnay { get; set; }
    }
}
