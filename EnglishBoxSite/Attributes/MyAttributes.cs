﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnglishBoxSite.Models;

namespace EnglishBoxSite.Attributes
{
    public class AdminRoleControl : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // action çalışmadan önce yapılacak işlemler

            var ent = new EnglishBoxDbEntities();
            if (HttpContext.Current.Session["KullaniciEmail"] == null)
            {
                filterContext.HttpContext.Response.Redirect("~/Home/Register");
            }
            else
            {
                string Eposta = HttpContext.Current.Session["KullaniciEmail"].ToString();
                var uye_rol = ent.Kullanicilar.FirstOrDefault(x => x.KullaniciEmail == Eposta).KullaniciRol;
                if (uye_rol != "Admin")
                {
                    filterContext.HttpContext.Response.Redirect("~/Home/Index");
                }
            }
            base.OnActionExecuting(filterContext);
        }

    }
}