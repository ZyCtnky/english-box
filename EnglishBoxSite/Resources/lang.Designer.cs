﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources
{

    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class lang {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal lang() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("EnglishBoxSite.Resources.lang", typeof(lang).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Açıklama.
        /// </summary>
        public static string Aciklama {
            get {
                return ResourceManager.GetString("Aciklama", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ad.
        /// </summary>
        public static string Ad {
            get {
                return ResourceManager.GetString("Ad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin.
        /// </summary>
        public static string Admin {
            get {
                return ResourceManager.GetString("Admin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ad Soyad.
        /// </summary>
        public static string AdSoyad {
            get {
                return ResourceManager.GetString("AdSoyad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ana Sayfa.
        /// </summary>
        public static string AnaSayfa {
            get {
                return ResourceManager.GetString("AnaSayfa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ara.
        /// </summary>
        public static string Ara {
            get {
                return ResourceManager.GetString("Ara", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Başlık.
        /// </summary>
        public static string Baslik {
            get {
                return ResourceManager.GetString("Baslik", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bilgilerimi Güncelle.
        /// </summary>
        public static string BilgilerimiGuncelle {
            get {
                return ResourceManager.GetString("BilgilerimiGuncelle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çıkış Yap.
        /// </summary>
        public static string CikisYap {
            get {
                return ResourceManager.GetString("CikisYap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Detaylar.
        /// </summary>
        public static string Detaylar {
            get {
                return ResourceManager.GetString("Detaylar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Devamını Oku.
        /// </summary>
        public static string DevaminiOku {
            get {
                return ResourceManager.GetString("DevaminiOku", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Diğer Tasarımları Gör.
        /// </summary>
        public static string DigerTasarimlar {
            get {
                return ResourceManager.GetString("DigerTasarimlar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dil Bilgisi Dersleri.
        /// </summary>
        public static string DilBilgisiDersleri {
            get {
                return ResourceManager.GetString("DilBilgisiDersleri", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dil Bilgisi ve Kelime.
        /// </summary>
        public static string DilBilgisiveKelime {
            get {
                return ResourceManager.GetString("DilBilgisiveKelime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Döküman.
        /// </summary>
        public static string Dokuman {
            get {
                return ResourceManager.GetString("Dokuman", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Düzenle.
        /// </summary>
        public static string Duzenle {
            get {
                return ResourceManager.GetString("Duzenle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş Yap.
        /// </summary>
        public static string GirisYap {
            get {
                return ResourceManager.GetString("GirisYap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İçeriklere ulaşabilmek için kayıt olmanız gerekmektedir..
        /// </summary>
        public static string GirisYazi {
            get {
                return ResourceManager.GetString("GirisYazi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Görsel.
        /// </summary>
        public static string Gorsel {
            get {
                return ResourceManager.GetString("Gorsel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Göster.
        /// </summary>
        public static string Goster {
            get {
                return ResourceManager.GetString("Goster", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Güncelle.
        /// </summary>
        public static string Guncelle {
            get {
                return ResourceManager.GetString("Guncelle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Günün Yayınları.
        /// </summary>
        public static string GununYayinlari {
            get {
                return ResourceManager.GetString("GununYayinlari", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hesap Ayarları.
        /// </summary>
        public static string HesapAyarlari {
            get {
                return ResourceManager.GetString("HesapAyarlari", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hesap Ayarlarına Dön.
        /// </summary>
        public static string HesapAyarlarinaDon {
            get {
                return ResourceManager.GetString("HesapAyarlarinaDon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hoş Geldiniz.
        /// </summary>
        public static string HosGeldiniz {
            get {
                return ResourceManager.GetString("HosGeldiniz", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kaydet.
        /// </summary>
        public static string Kaydet {
            get {
                return ResourceManager.GetString("Kaydet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kayıt Ol.
        /// </summary>
        public static string KayitOl {
            get {
                return ResourceManager.GetString("KayitOl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kelime Kartı.
        /// </summary>
        public static string KelimeKarti {
            get {
                return ResourceManager.GetString("KelimeKarti", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kelime Kartları.
        /// </summary>
        public static string KelimeKartlari {
            get {
                return ResourceManager.GetString("KelimeKartlari", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcılar.
        /// </summary>
        public static string Kullanicilar {
            get {
                return ResourceManager.GetString("Kullanicilar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcı Sayfası.
        /// </summary>
        public static string KullaniciSayfasi {
            get {
                return ResourceManager.GetString("KullaniciSayfasi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Menü.
        /// </summary>
        public static string Menü {
            get {
                return ResourceManager.GetString("Menü", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Meslek.
        /// </summary>
        public static string Meslek {
            get {
                return ResourceManager.GetString("Meslek", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ödevler.
        /// </summary>
        public static string Odevler {
            get {
                return ResourceManager.GetString("Odevler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Okul.
        /// </summary>
        public static string Okul {
            get {
                return ResourceManager.GetString("Okul", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Okuma Parçaları.
        /// </summary>
        public static string OkumaParcalari {
            get {
                return ResourceManager.GetString("OkumaParcalari", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifre.
        /// </summary>
        public static string Sifre {
            get {
                return ResourceManager.GetString("Sifre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifre Değiştir.
        /// </summary>
        public static string SifreDegistir {
            get {
                return ResourceManager.GetString("SifreDegistir", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sil.
        /// </summary>
        public static string Sil {
            get {
                return ResourceManager.GetString("Sil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yazılı Hazırlık soruları.
        /// </summary>
        public static string SinavaHazirlikSorulari {
            get {
                return ResourceManager.GetString("SinavaHazirlikSorulari", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sınıf.
        /// </summary>
        public static string Sinif {
            get {
                return ResourceManager.GetString("Sinif", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Site Tasarımı.
        /// </summary>
        public static string SiteTasarim {
            get {
                return ResourceManager.GetString("SiteTasarim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Eğer Siz Hayallerinizi İnşa Etmek İçin Çalışmazsanız, Başka Birilerinin Hayallerini İnşa Etmek İçin Çalışmak Zorunda Kalırsınız..
        /// </summary>
        public static string Slogan {
            get {
                return ResourceManager.GetString("Slogan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Son Tarih.
        /// </summary>
        public static string SonTarih {
            get {
                return ResourceManager.GetString("SonTarih", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Son Yayınlar.
        /// </summary>
        public static string SonYayinlar {
            get {
                return ResourceManager.GetString("SonYayinlar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sosyal Medya Hesaplarımız.
        /// </summary>
        public static string SosyalMedya {
            get {
                return ResourceManager.GetString("SosyalMedya", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Soyad.
        /// </summary>
        public static string Soyad {
            get {
                return ResourceManager.GetString("Soyad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tarih.
        /// </summary>
        public static string Tarih {
            get {
                return ResourceManager.GetString("Tarih", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Yazılı Hazırlık Soruları Ekle.
        /// </summary>
        public static string YeniHazirlikSorulari {
            get {
                return ResourceManager.GetString("YeniHazirlikSorulari", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Hesap Oluştur.
        /// </summary>
        public static string YeniHesap {
            get {
                return ResourceManager.GetString("YeniHesap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Kart Oluştur.
        /// </summary>
        public static string YeniKart {
            get {
                return ResourceManager.GetString("YeniKart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Kullanici Ekle.
        /// </summary>
        public static string YeniKullanici {
            get {
                return ResourceManager.GetString("YeniKullanici", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Ödev Ekle.
        /// </summary>
        public static string YeniOdev {
            get {
                return ResourceManager.GetString("YeniOdev", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Dil Dil Bilgisi Yazisi Ekle.
        /// </summary>
        public static string YeniYazi {
            get {
                return ResourceManager.GetString("YeniYazi", resourceCulture);
            }
        }
    }
}
