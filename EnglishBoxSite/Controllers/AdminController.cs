﻿using EnglishBoxSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace EnglishBoxSite.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult Index()
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                AnaSayfaDTO anaSayfa = new AnaSayfaDTO();
                anaSayfa.KelimeKarti = ent.KelimeKartlari.OrderByDescending(x => x.KartTarih).ToList();
                anaSayfa.DilBilgisiYazi = ent.DilBilgisiYazilari.OrderByDescending(x => x.YaziTarih).ToList();
                return View(anaSayfa);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult Cikis()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        #region /*Kullanıcılar*/
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult Kullanicilar(int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var kullanicilar = ent.Kullanicilar.ToList();
                return View(kullanicilar.ToPagedList(pageNumber, pageSize));
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult KullaniciGoster(int KullaniciID)
        {
            using(EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _kullanici = ent.Kullanicilar.Where(x => x.KullaniciID == KullaniciID).FirstOrDefault();
                return View(_kullanici);
            } 
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult KullaniciDuzenle(int KullaniciID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _kullanici = ent.Kullanicilar.Where(x => x.KullaniciID == KullaniciID).FirstOrDefault();
                return View(_kullanici);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult KullaniciDuzenle(Kullanicilar kullanici)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    var _kullaniciDuzenle = ent.Kullanicilar.Where(x => x.KullaniciID == kullanici.KullaniciID).FirstOrDefault();
                    _kullaniciDuzenle.KullaniciAdi = kullanici.KullaniciAdi;
                    _kullaniciDuzenle.KullaniciSoyadi = kullanici.KullaniciSoyadi;
                    _kullaniciDuzenle.KullaniciEmail = kullanici.KullaniciEmail;
                    _kullaniciDuzenle.KullaniciOkul = kullanici.KullaniciOkul;
                    _kullaniciDuzenle.KullaniciRol = kullanici.KullaniciRol;
                    _kullaniciDuzenle.KullaniciAktivasyon = kullanici.KullaniciAktivasyon;
                    _kullaniciDuzenle.KullaniciKayitTarihi = kullanici.KullaniciKayitTarihi;
                    _kullaniciDuzenle.Onay = kullanici.Onay;
                    ent.SaveChanges();
                    return RedirectToAction("Kullanicilar", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Güncellerken hata oluştu " + ex.Message);
                }
            }

        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult KullaniciSil(int KullaniciID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _kullanici = ent.Kullanicilar.Where(x => x.KullaniciID == KullaniciID).FirstOrDefault();
                return View(_kullanici);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult KullaniciSil(Kullanicilar kullanici)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    ent.Kullanicilar.Remove(ent.Kullanicilar.First(d => d.KullaniciID == kullanici.KullaniciID));
                    ent.SaveChanges();
                    return RedirectToAction("Kullanicilar", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
        }
        #endregion


        #region /*Dil Bilgisi Yazilari*/
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult AdminDilBilgisiYazilari(int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var yazilar = ent.DilBilgisiYazilari.ToList();
                return View(yazilar.ToPagedList(pageNumber, pageSize));
            }
        }

        [ValidateInput(false)]
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult YaziEkle(DilBilgisiYazilari yazi, HttpPostedFileBase file)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    DilBilgisiYazilari _yazi = new DilBilgisiYazilari();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _yazi.YaziGorsel = memoryStream.ToArray();
                    }
                    _yazi.YaziBaslik = yazi.YaziBaslik;
                    _yazi.YaziIcerik = yazi.YaziIcerik;
                    _yazi.YaziTarih = DateTime.Now;
                    ent.DilBilgisiYazilari.Add(_yazi);
                    ent.SaveChanges();
                    return RedirectToAction("AdminDilBilgisiYazilari", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Eklerken hata oluştu");
                }
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult YaziEkle()
        {
            return View();
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult YaziGoster(int YaziID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _yazi = ent.DilBilgisiYazilari.Where(x => x.YaziID == YaziID).FirstOrDefault();
                return View(_yazi);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult YaziDuzenle(int YaziID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _yazi = ent.DilBilgisiYazilari.Where(x => x.YaziID == YaziID).FirstOrDefault();
                return View(_yazi);
            }
        }
        [ValidateInput(false)]
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult YaziDuzenle(DilBilgisiYazilari yazi, HttpPostedFileBase file)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    var _yaziDuzenle = ent.DilBilgisiYazilari.Where(x => x.YaziID == yazi.YaziID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _yaziDuzenle.YaziGorsel = memoryStream.ToArray();
                    }
                    _yaziDuzenle.YaziBaslik = yazi.YaziBaslik;
                    _yaziDuzenle.YaziIcerik = yazi.YaziIcerik;
                    _yaziDuzenle.YaziTarih = yazi.YaziTarih;
                    ent.SaveChanges();
                    return RedirectToAction("AdminDilBilgisiYazilari", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Güncellerken hata oluştu " + ex.Message);
                }
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult YaziSil(int YaziID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _yazi = ent.DilBilgisiYazilari.Where(x => x.YaziID == YaziID).FirstOrDefault();
                return View(_yazi);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult YaziSil(DilBilgisiYazilari yazi)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    ent.DilBilgisiYazilari.Remove(ent.DilBilgisiYazilari.First(d => d.YaziID == yazi.YaziID));
                    ent.SaveChanges();
                    return RedirectToAction("AdminDilBilgisiYazilari", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
        }
        #endregion

        #region /*Kelime Kartları*/
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult AKelimeKartlari(int? page)
        {
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var kartlar = ent.KelimeKartlari.ToList();
                return View(kartlar.ToPagedList(pageNumber, pageSize));
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult KartEkle(KelimeKartlari kart, HttpPostedFileBase file)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    KelimeKartlari _kart = new KelimeKartlari();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _kart.KartGorsel = memoryStream.ToArray();
                    }
                    _kart.KartBaslik = kart.KartBaslik;
                    _kart.KartTarih = DateTime.Now;
                    ent.KelimeKartlari.Add(_kart);
                    ent.SaveChanges();
                    return RedirectToAction("AKelimeKartlari", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Eklerken hata oluştu");
                }
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult KartEkle()
        {
            return View();
        }

        public ActionResult KartGoster(int KartID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _kart = ent.KelimeKartlari.Where(x => x.KartID == KartID).FirstOrDefault();
                return View(_kart);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult KartDuzenle(int KartID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _kart = ent.KelimeKartlari.Where(x => x.KartID == KartID).FirstOrDefault();
                return View(_kart);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult KartDuzenle(KelimeKartlari kart, HttpPostedFileBase file)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    var _kartDuzenle = ent.KelimeKartlari.Where(x => x.KartID == kart.KartID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _kartDuzenle.KartGorsel = memoryStream.ToArray();
                    }
                    _kartDuzenle.KartBaslik = kart.KartBaslik;
                    _kartDuzenle.KartTarih = kart.KartTarih;
                    ent.SaveChanges();
                    return RedirectToAction("AKelimeKartlari", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Güncellerken hata oluştu " + ex.Message);
                }
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult KartSil(int KartID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _kart = ent.KelimeKartlari.Where(x => x.KartID == KartID).FirstOrDefault();
                return View(_kart);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult KartSil(KelimeKartlari kart)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    ent.KelimeKartlari.Remove(ent.KelimeKartlari.First(d => d.KartID == kart.KartID));
                    ent.SaveChanges();
                    return RedirectToAction("AKelimeKartlari", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
        }
        #endregion

        #region /*Yazılı Hazırlık Soruları*/
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult AHazirlikSorulari(int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var yazili = ent.YaziliSorulari.ToList();
                return View(yazili.ToPagedList(pageNumber, pageSize));
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult YaziliHazirlikEkle(YaziliSorulari yazili, HttpPostedFileBase file)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    YaziliSorulari _yazili = new YaziliSorulari();
                    _yazili.YaziliBaslik = yazili.YaziliBaslik;
                    _yazili.YaziliAciklama = yazili.YaziliAciklama;
                    _yazili.YaziliSinif = yazili.YaziliSinif;
                    _yazili.YaziliTarih = DateTime.Now;
                    _yazili.YaziliDokuman = yazili.YaziliDokuman;
                    ent.YaziliSorulari.Add(_yazili);
                    ent.SaveChanges();
                    return RedirectToAction("AHazirlikSorulari", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Eklerken hata oluştu");
                }
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult YaziliHazirlikEkle()
        {
            return View();
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult YaziliHazirlikDuzenle(int YaziliID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _yazili = ent.YaziliSorulari.Where(x => x.YaziliID == YaziliID).FirstOrDefault();
                return View(_yazili);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult YaziliHazirlikDuzenle(YaziliSorulari yazili, HttpPostedFileBase file)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    var _yaziliDuzenle = ent.YaziliSorulari.Where(x => x.YaziliID == yazili.YaziliID).FirstOrDefault();
                    _yaziliDuzenle.YaziliBaslik = yazili.YaziliBaslik;
                    _yaziliDuzenle.YaziliAciklama = yazili.YaziliAciklama;
                    _yaziliDuzenle.YaziliSinif = yazili.YaziliSinif;
                    _yaziliDuzenle.YaziliTarih = yazili.YaziliTarih;
                    _yaziliDuzenle.YaziliAciklama = yazili.YaziliAciklama;
                    _yaziliDuzenle.YaziliDokuman = yazili.YaziliDokuman;
                    ent.SaveChanges();
                    return RedirectToAction("AHazirlikSorulari", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Güncellerken hata oluştu " + ex.Message);
                }
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult YaziliHazirlikSil(int YaziliID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _yazili = ent.YaziliSorulari.Where(x => x.YaziliID == YaziliID).FirstOrDefault();
                return View(_yazili);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult YaziliHazirlikSil(YaziliSorulari yazili)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    ent.YaziliSorulari.Remove(ent.YaziliSorulari.First(d => d.YaziliID == yazili.YaziliID));
                    ent.SaveChanges();
                    return RedirectToAction("AHazirlikSorulari", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
        }
        #endregion

        #region /*Odevler*/
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult AOdevler(int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var odev = ent.Odevler.ToList();
                return View(odev.ToPagedList(pageNumber, pageSize));
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult OdevEkle(Odevler odev, HttpPostedFileBase file)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    Odevler _odev = new Odevler();
                    _odev.OdevBaslik = odev.OdevBaslik;
                    _odev.OdevAciklama = odev.OdevAciklama;
                    _odev.OdevSinif = odev.OdevSinif;
                    _odev.OdevTarih = DateTime.Now;
                    _odev.OdevTeslimTarih = odev.OdevTeslimTarih;
                    _odev.OdevDokuman = odev.OdevDokuman;
                    ent.Odevler.Add(_odev);
                    ent.SaveChanges();
                    return RedirectToAction("AOdevler", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Eklerken hata oluştu");
                }
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult OdevEkle()
        {
            return View();
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult OdevDuzenle(int OdevID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _odev = ent.Odevler.Where(x => x.OdevID == OdevID).FirstOrDefault();
                return View(_odev);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult OdevDuzenle(Odevler odev, HttpPostedFileBase file)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    var _odevDuzenle = ent.Odevler.Where(x => x.OdevID == odev.OdevID).FirstOrDefault();
                    _odevDuzenle.OdevBaslik = odev.OdevBaslik;
                    _odevDuzenle.OdevAciklama = odev.OdevAciklama;
                    _odevDuzenle.OdevSinif = odev.OdevSinif;
                    _odevDuzenle.OdevTarih = odev.OdevTarih;
                    _odevDuzenle.OdevTeslimTarih = odev.OdevTeslimTarih;
                    _odevDuzenle.OdevDokuman = odev.OdevDokuman;
                    ent.SaveChanges();
                    return RedirectToAction("AOdevler", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Güncellerken hata oluştu " + ex.Message);
                }
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult OdevSil(int OdevID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var _odev = ent.Odevler.Where(x => x.OdevID == OdevID).FirstOrDefault();
                return View(_odev);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult OdevSil(Odevler odev)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    ent.Odevler.Remove(ent.Odevler.First(d => d.OdevID == odev.OdevID));
                    ent.SaveChanges();
                    return RedirectToAction("AOdevler", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
        }
        #endregion

        #region /*Hesap Ayarları*/
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult AHesapAyarlari(int KullaniciID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var kullanici = ent.Kullanicilar.Where(x => x.KullaniciID == KullaniciID).FirstOrDefault();
                return View(kullanici);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult AHesapBilgileriniGuncelle(int KullaniciID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var kullanici = ent.Kullanicilar.Where(x => x.KullaniciID == KullaniciID).FirstOrDefault();
                return View(kullanici);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult AHesapBilgileriniGuncelle(Kullanicilar kullanici)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    var _kullaniciDuzenle = ent.Kullanicilar.Where(x => x.KullaniciID == kullanici.KullaniciID).FirstOrDefault();
                    _kullaniciDuzenle.KullaniciAdi = kullanici.KullaniciAdi;
                    _kullaniciDuzenle.KullaniciSoyadi = kullanici.KullaniciSoyadi;
                    _kullaniciDuzenle.KullaniciEmail = kullanici.KullaniciEmail;
                    _kullaniciDuzenle.KullaniciMeslek = kullanici.KullaniciMeslek;
                    _kullaniciDuzenle.KullaniciOkul = kullanici.KullaniciOkul;
                    ent.SaveChanges();
                    return RedirectToAction("Index", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Güncellerken hata oluştu " + ex.Message);
                }
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        public ActionResult ASifreDegistir(int KullaniciID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var kullanici = ent.Kullanicilar.Where(x => x.KullaniciID == KullaniciID).FirstOrDefault();
                return View(kullanici);
            }
        }
        [EnglishBoxSite.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult ASifreDegistir(Kullanicilar kullanici)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    var _kullaniciDuzenle = ent.Kullanicilar.Where(x => x.KullaniciID == kullanici.KullaniciID).FirstOrDefault();
                    string sifre = FormsAuthentication.HashPasswordForStoringInConfigFile(kullanici.KullaniciSifre, "md5");


                    if(_kullaniciDuzenle.KullaniciSifre == sifre)
                    {
                        string yeniSifre = FormsAuthentication.HashPasswordForStoringInConfigFile(kullanici.yeniSifre, "md5");
                        string yeniSifreOnay = FormsAuthentication.HashPasswordForStoringInConfigFile(kullanici.yeniSifreOnay, "md5");

                        _kullaniciDuzenle.yeniSifre = kullanici.yeniSifre;
                        _kullaniciDuzenle.yeniSifreOnay = kullanici.yeniSifreOnay;

                        if(kullanici.yeniSifre == kullanici.yeniSifreOnay)
                        {
                            _kullaniciDuzenle.KullaniciSifre = yeniSifre;
                            ent.SaveChanges();
                            return RedirectToAction("Index", "Admin");
                        }
                        else
                        {
                            throw new Exception("Şifreler Uyuşmuyor.");
                        }
                    }
                    else
                    {
                        throw new Exception("Eski Şifre Yanlış.");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Güncellerken hata oluştu " + ex.Message);
                }
            }
        }

        #endregion
        public class AnaSayfaDTO
        {
            public List<DilBilgisiYazilari> DilBilgisiYazi { get; set; }
            public List<KelimeKartlari> KelimeKarti { get; set; }
            public List<Odevler> Odev { get; set; }
            public List<KisaSinavlar> KisaSinav { get; set; }
            public List<Mesajlar> Mesaj { get; set; }
            public List<YaziliSorulari> YaziliSorulari { get; set; }

        }
    }
}