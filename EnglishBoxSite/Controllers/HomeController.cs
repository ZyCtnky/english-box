﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnglishBoxSite.Models;
using System.Web.Security;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using PagedList;

namespace EnglishBoxSite.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

        public ActionResult KullaniciAnaSayfa()
        {
            using(EnglishBoxDbEntities ent=new EnglishBoxDbEntities())
            {
                AnaSayfaDTO anaSayfa = new AnaSayfaDTO();
                anaSayfa.KelimeKarti = ent.KelimeKartlari.OrderByDescending(x => x.KartTarih).ToList();
                anaSayfa.DilBilgisiYazi = ent.DilBilgisiYazilari.OrderByDescending(x => x.YaziTarih).ToList();
                return View(anaSayfa);
            }
        }

        
        public ActionResult DilBilgisiDersleri(int? page)
        {
            using(EnglishBoxDbEntities ent=new EnglishBoxDbEntities())
            {
                int pageSize = 3;
                int pageNumber = (page ?? 1);
                List<DilBilgisiYazilari> dilBilgisi = ent.DilBilgisiYazilari.OrderByDescending(x => x.YaziTarih).ToList();

                return View(dilBilgisi.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult DilBilgisiDetay(int YaziID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                DilBilgisiYazilari yaziDetay = ent.DilBilgisiYazilari.FirstOrDefault(x => x.YaziID == YaziID);
                return View(yaziDetay);
            }
        }

        public ActionResult KelimeKartlari(int? page)
        {
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                List<KelimeKartlari> kelimeKartlari = ent.KelimeKartlari.OrderByDescending(x => x.KartTarih).ToList();
                return View(kelimeKartlari.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult YaziliHazirlikSorulari(int? page)
        {
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                List<YaziliSorulari> yaziliSorulari = ent.YaziliSorulari.OrderByDescending(x => x.YaziliTarih).ToList();
                return View(yaziliSorulari.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult Odevler(int? page)
        {
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                List<Odevler> odevler = ent.Odevler.OrderByDescending(x => x.OdevTarih).ToList();
                return View(odevler.ToPagedList(pageNumber, pageSize));
            }
        }

        

        public ActionResult RegisterMessage()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult UyeEkle(RegisterViewModel RegisterModel)
        {
            var ent = new EnglishBoxDbEntities();
            Kullanicilar _kursogrenci = new Kullanicilar();
            string aktivasyonkodu = Guid.NewGuid().ToString("N").Substring(0, 20).ToUpper();
            _kursogrenci.KullaniciAdi = RegisterModel.Ad;
            _kursogrenci.KullaniciSoyadi = RegisterModel.SoyAd;
            _kursogrenci.KullaniciOkul = RegisterModel.Okul;
            _kursogrenci.KullaniciEmail = RegisterModel.Email;
            _kursogrenci.KullaniciSifre = FormsAuthentication.HashPasswordForStoringInConfigFile(RegisterModel.Password, "md5");
            _kursogrenci.KullaniciAktivasyon = aktivasyonkodu;
            _kursogrenci.Onay = false;
            _kursogrenci.KullaniciRol = "Kullanici";
            _kursogrenci.KullaniciKayitTarihi = DateTime.Now;
            ent.Kullanicilar.Add(_kursogrenci);
            ent.SaveChanges();
            string mailIcerik = "ENGLISH BOX AİLESİNE HOSGELDİNIZ<br>www.englishbox.com a kayıt olduğunuz için teşekkür ederiz. <br> Üyeliğinizi onaylamak için doğrulamak için aşağıdaki linke tıklayınız.<br> ";
            //mailIcerik += "<a href=\"http://englishbox.sakarya.edu.tr/Home/Aktivasyon?Eposta=" + RegisterModel.Email + "&AktivasyonKodu=" + aktivasyonkodu + "\" style=\"text-decoration:underline; color:#3ba5d8;\">Üyeliğimi Aktifleştir</a>";
            mailIcerik += "<a href=\"http://englishbox.sakarya.edu.tr/Home/Aktivasyon?Eposta=" + RegisterModel.Email + "&AktivasyonKodu=" + aktivasyonkodu + "\" style=\"text-decoration:underline; color:#3ba5d8;\">Üyeliğimi Aktifleştir</a>";
            MailGonder(RegisterModel.Email, "English Box ~ Üyelik Aktivasyon", mailIcerik);
            return View("RegisterMessage");
        }

        [HttpPost]
        public ActionResult UyeGiris(LoginViewModel LoginModel)
        {
            if (ModelState.IsValid)
            {
                var ent = new EnglishBoxDbEntities();
                string sifre = FormsAuthentication.HashPasswordForStoringInConfigFile(LoginModel.Password, "md5");
                var uye = ent.Kullanicilar.Where(x => x.KullaniciEmail == LoginModel.Email && x.KullaniciSifre == sifre /*&& x.Onay == true*/).FirstOrDefault();
                if (uye != null)
                {
                    Session["KullaniciEmail"] = LoginModel.Email;
                    Session["KullaniciRol"] = uye.KullaniciRol;
                    Session["KullaniciAdisoyadi"] = uye.KullaniciAdi + " " +uye.KullaniciSoyadi;
                    Session["KullaniciID"] = uye.KullaniciID;

                    //return RedirectToAction("KullaniciAnaSayfa", "Home");

                    if (uye.KullaniciRol == "Admin")
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    else
                    {
                        return RedirectToAction("KullaniciAnaSayfa", "Home");
                    }
                }
                else
                {
                    TempData["Mesaj"] = "Kullanıcı Adı veya şifre yanlış, eposta aktivasyon maili onaylanmamıştır";
                }
            }
            return View("Login");
        }

        public static void MailGonder(string kime, string baslik, string mesajIcerik)
        {
            string server = "smtp.gmail.com";
            string kadi = "zyctnky.02@gmail.com";
            string sifre = "xxxxxxxxxx";
            int port = 587;
            SmtpClient smtpclient = new SmtpClient();
            smtpclient.Port = port; //Smtp Portu (Sunucuya Göre Değişir)
            smtpclient.Host = server; //Smtp Hostu (Gmail smtp adresi bu şekilde)
            smtpclient.EnableSsl = false; //Sunucunun SSL kullanıp kullanmadıgı
            smtpclient.Credentials = new NetworkCredential(kadi, sifre); //Gmail Adresiniz ve Şifreniz
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(kadi, "English Box"); //Gidecek Mail Adresi ve Görünüm Adınız
            mail.To.Add(kime); //Kime Göndereceğiniz
            mail.Subject = baslik; //Emailin Konusu
            mail.Body = mesajIcerik; //Mesaj İçeriği
            mail.IsBodyHtml = true; //Mesajınızın Gövdesinde HTML destegininin olup olmadıgı

            try
            {
                smtpclient.Send(mail);
                mail.Dispose();
            }
            catch
            {
            }
        }

        public ActionResult Aktivasyon(string eposta, string aktivasyonkodu)
        {
            var ent = new EnglishBoxDbEntities();
            var uye = ent.Kullanicilar.Where(x => x.KullaniciEmail == eposta && x.KullaniciAktivasyon == aktivasyonkodu).FirstOrDefault();
            if (uye != null)
            {
                uye.Onay = true;
                ent.SaveChanges();
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Cikis()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        public static string RoleGetir(string eposta)
        {
            var ent = new EnglishBoxDbEntities();
            var uye_rol = ent.Kullanicilar.FirstOrDefault(x => x.KullaniciEmail == eposta).KullaniciRol;
            return uye_rol;
        }

        #region /*Hesap Ayarları*/
        public ActionResult HesapAyarlari(int KullaniciID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var kullanici = ent.Kullanicilar.Where(x => x.KullaniciID == KullaniciID).FirstOrDefault();
                return View(kullanici);
            }
        }
        
        public ActionResult HesapBilgileriniGuncelle(int KullaniciID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var kullanici = ent.Kullanicilar.Where(x => x.KullaniciID == KullaniciID).FirstOrDefault();
                return View(kullanici);
            }
        }
       
        [HttpPost]
        public ActionResult HesapBilgileriniGuncelle(Kullanicilar kullanici)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    var _kullaniciDuzenle = ent.Kullanicilar.Where(x => x.KullaniciID == kullanici.KullaniciID).FirstOrDefault();
                    _kullaniciDuzenle.KullaniciAdi = kullanici.KullaniciAdi;
                    _kullaniciDuzenle.KullaniciSoyadi = kullanici.KullaniciSoyadi;
                    _kullaniciDuzenle.KullaniciEmail = kullanici.KullaniciEmail;
                    _kullaniciDuzenle.KullaniciMeslek = kullanici.KullaniciMeslek;
                    _kullaniciDuzenle.KullaniciOkul = kullanici.KullaniciOkul;
                    ent.SaveChanges();
                    return RedirectToAction("KullaniciAnaSayfa", "Home");
                }
                catch (Exception ex)
                {
                    throw new Exception("Güncellerken hata oluştu " + ex.Message);
                }
            }
        }
       
        public ActionResult SifreDegistir(int KullaniciID)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                var kullanici = ent.Kullanicilar.Where(x => x.KullaniciID == KullaniciID).FirstOrDefault();
                return View(kullanici);
            }
        }
       
        [HttpPost]
        public ActionResult SifreDegistir(Kullanicilar kullanici)
        {
            using (EnglishBoxDbEntities ent = new EnglishBoxDbEntities())
            {
                try
                {
                    var _kullaniciDuzenle = ent.Kullanicilar.Where(x => x.KullaniciID == kullanici.KullaniciID).FirstOrDefault();
                    string sifre = FormsAuthentication.HashPasswordForStoringInConfigFile(kullanici.KullaniciSifre, "md5");


                    if (_kullaniciDuzenle.KullaniciSifre == sifre)
                    {
                        string yeniSifre = FormsAuthentication.HashPasswordForStoringInConfigFile(kullanici.yeniSifre, "md5");
                        string yeniSifreOnay = FormsAuthentication.HashPasswordForStoringInConfigFile(kullanici.yeniSifreOnay, "md5");

                        _kullaniciDuzenle.yeniSifre = kullanici.yeniSifre;
                        _kullaniciDuzenle.yeniSifreOnay = kullanici.yeniSifreOnay;

                        if (kullanici.yeniSifre == kullanici.yeniSifreOnay)
                        {
                            _kullaniciDuzenle.KullaniciSifre = yeniSifre;
                            ent.SaveChanges();
                            return RedirectToAction("KullaniciAnaSayfa", "Home");
                        }
                        else
                        {
                            throw new Exception("Şifreler Uyuşmuyor.");
                        }
                    }
                    else
                    {
                        throw new Exception("Eski Şifre Yanlış.");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Güncellerken hata oluştu " + ex.Message);
                }
            }
        }

        #endregion

        public class AnaSayfaDTO
        {
            public List<DilBilgisiYazilari> DilBilgisiYazi { get; set; }
            public List<KelimeKartlari> KelimeKarti { get; set; }
            public List<Odevler> Odev { get; set; }
            public List<KisaSinavlar> KisaSinav { get; set; }
            public List<Mesajlar> Mesaj { get; set; }
            public List<YaziliSorulari> YaziliSorulari { get; set; }
            
        }
    }
}